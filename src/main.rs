use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::str::FromStr;
use roxmltree::*;

fn process(xml_file_path: &str, output_path: &str) {
    let mut buffer = String::new();
    let mut file = File::open(&xml_file_path).unwrap();
    let mut output_file = File::create(output_path).unwrap();
    let mut ucd_table: Vec<String> = Vec::with_capacity(0xFFFD);
    for _ in 0..0xFFFD {
        ucd_table.push("".to_string());
    }
    ucd_table.fill(String::from(""));
    file.read_to_string(&mut buffer).unwrap();
    println!("parsing started");
    let doc = Document::parse(&buffer);
    println!("parsing finished");
    for node in doc.expect("roxmltree: Failed to parse the document")
        .root().descendants() {
        if node.has_attribute("cp") && node.has_attribute("na") {
            let index = usize::from_str_radix(node.attribute("cp").unwrap(), 16).unwrap();
            if index < ucd_table.len() {
                ucd_table[index]
                    = String::from(node.attribute("na").unwrap());
            } else {
                break;
            }
        }
    }
    for entry in ucd_table {
        output_file.write_fmt(format_args!("\"{}\",", entry)).unwrap();
    }
}

fn process_args() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        println!("Usage ucdxmltoh [file path] [output path]\
        \n Expects a path to the Unicode database in XML format, writes to output file");
    } else {
        if Path::new(&args[1]).is_file() {
            process(&args[1], &args[2]);
        }
    }
}

fn main() {
    process_args()
}