## ucdxmltoh
A tool I wrote, in Rust, for bfontview to aid in making a lookup table for UTF-16.
Download the flat UCD from ucd.all.flat.zip, at https://www.unicode.org/Public/14.0.0/ucdxml/.

Written by Ben Cottrell.

### Wishlist
 - Greater efficiency, with a XML parser supporting multiple threads and cores.